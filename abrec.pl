#!/usr/bin/perl

#----------------------------------------------------------------------
#
# abrec.pl -- Torsten Crass, 2013 - 2015
#
# Record (part of) an aax audiobook to a flac file
#
# Call 'abrec.pl --help' for more information.
#
# This program's homepage:
# http://www.tcrass.de/computer/tools/media/abtools
#
# LICENSE
# -------
#
# This program is distributed under the terms of the GNU GPL,
# version 2 (see http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
#
#----------------------------------------------------------------------


use strict;
use warnings;

use POSIX ':sys_wait_h';

use SysUtils;
use ABTools;

my $WINE_BUG = 0;
my $PART_NO = 1;
my $REC_COMMAND = 'rec "%o" silence 1 0.1 0.5% 1 %t 0.5%';
my $TERMINATE_AFTER_SILENCE = '15.0';
my $PLAY_COMMAND = 'wine Manager.exe 2>&1 > /dev/null';

my $OPT_DEFS = {
  'wine-bug|b' => \$WINE_BUG,
  'part|p=' => \$PART_NO,
  'rec-command|r=' => \$REC_COMMAND,
  'terminate-after-silence|t=' => \$TERMINATE_AFTER_SILENCE,
  'play-command|y=' => \$PLAY_COMMAND
};

ABTools::init($OPT_DEFS, \&printUsage);

my $PART_DIR = &ABTools::createPartDir($PART_NO);

my $OUT = &ABTools::partRecPath($PART_NO);
my $recCmd = $REC_COMMAND;
$recCmd =~ s/%o/$OUT/g;
my $terminateAfterSilence = sprintf('%.3f', $TERMINATE_AFTER_SILENCE);
$recCmd =~ s/%t/$terminateAfterSilence/g;

if ($WINE_BUG) {

  (my $playPid, my $err) = &SysUtils::execSilentlyInBackground($PLAY_COMMAND);
  die($err) if $err;

  print(
    "Please play a few seconds of audio and return to the audio book's start.\n",
    "Hit a key when done.\n"
  );
  &SysUtils::getKeystroke();

  print("You may now start playing back the audio book.\n");
  &SysUtils::execCommand($recCmd);

  kill(-9, $playPid);

}
else {

  (my $recPid, my $err) = &SysUtils::execSilentlyInBackground($recCmd);
  die($err) if $err;

  print("Now go ahead and play your audio book *from the beginning*.\n");
  &SysUtils::execCommand($PLAY_COMMAND);

  my $t0 = time();
  my $donePid = $recPid;
  do {
    $donePid = waitpid($recPid, WNOHANG);
    sleep(1);
  } while (($donePid >= 0) && ((time() - $t0) < ($TERMINATE_AFTER_SILENCE + 2)));
  if ($donePid >= 0) {
    kill(-9, $recPid);
  }

}



sub printUsage {
  print <<EOT;

abrec.pl -- Record (part of) an aax audiobook to a flac file
Version $VERSION
-----------------------------------------------------------------------
Usage: abrec.pl [options]
with possible options:
-?|--help           : Show this message.
-b|--wine-bug       : Use a less comfortable way of calling external
                      helper programs in order to circumvent an ALSA
                      recording bug in Wine.
-c|--config-path    : Path to an alternative config file.
-p|--part-no        : Part number of a multi-part audiobook; should be
                      '1' for single-part audiobooks. [$PART_NO]
-r|--rec-command    : What to execute in order to record the audiobook
                      played by the --play-command. The following
                      placeholders are understood: %o = Path to
                      output file; %t = value of -t parameter.
                      [$REC_COMMAND]
-t|--terminate-after-silence : After how many seconds of silence the
                      recording is to be considered completed. [$TERMINATE_AFTER_SILENCE]
-y|--play-command:  : What to execute in order to play the audiobook.
                      [$PLAY_COMMAND]
-v|--verbose        : Talk a lot.
-w|--working-dir    : Where the recorded flac files are to be stored.
                      [$WORKING_DIR]
Requires ABTools.pm and SysUtils.pm to be in PERL5LIB and usually
relies on wine and sox/rec, which should be installed and in PATH.

EOT
}
