package ABTools;

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw($VERSION $HELP $CONFIG_PATH $DEBUG $VERBOSE $WORKING_DIR $CONFIG $COMMON_CONFIG &report);

use strict;
use warnings;

our $VERSION = '0.1.3';

use SysUtils;

our $HELP = 0;
our $CONFIG_PATH = 'abtools';
our $DEBUG = 0;
our $VERBOSE = 0;
our $WORKING_DIR = "$SysUtils::CURRDIR${SysUtils::DIRSEP}work";

our $CONFIG = undef;
our $COMMON_CONFIG = undef;

my $CONFIG_SECTION = $SysUtils::DEFAULT_CONFIG_SECTION;
if ($0 =~ m/ab([^.]+)\.pl/) {
  $CONFIG_SECTION = $1;
}

my $OPTS;

my $PRINT_USAGE = undef;

sub init {
  my $optDefs = shift;
  $PRINT_USAGE = shift;

  $optDefs->{'help|?'} = \$HELP;
  $optDefs->{'config-path|c='} = \$CONFIG_PATH;
  $optDefs->{'working-dir|d='} = \$WORKING_DIR;
  $optDefs->{'debug|D'} = \$DEBUG;
  $optDefs->{'verbose|v'} = \$VERBOSE;

  my $optDefaults = {};
  my $optNames = {};
  foreach my $optDef (keys(%{$optDefs})) {
    my $optName = $optDef;
    $optName =~ s/\|.*$//;
    $optNames->{$optDef} = $optName;
    $optDefaults->{$optName} = ${$optDefs->{$optDef}};
    ${$optDefs->{$optDef}} = undef;
  }

  (my $opts, my $args, my $err) = &SysUtils::parseOptions($optDefs);
  if ($err) {
    &printUsage($err);
  }
  else {
    $CONFIG_PATH = $opts->{'config-path'} || $optDefaults->{'config-path'};
    ($COMMON_CONFIG, $err, $CONFIG) = &SysUtils::readConfigFile($CONFIG_PATH);

    foreach my $optDef (keys(%{$optDefs})) {
      my $optName = $optNames->{$optDef};
      unless (defined($opts->{$optName})) {
        if (exists($CONFIG->{$CONFIG_SECTION}->{$optName})) {
          $opts->{$optName} = $CONFIG->{$CONFIG_SECTION}->{$optName}
        }
        elsif (exists($COMMON_CONFIG->{$optName})) {
          $opts->{$optName} = $COMMON_CONFIG->{$optName}
        }
        else {
          $opts->{$optName} = $optDefaults->{$optName}
        }
      }
      ${$optDefs->{$optDef}} = $opts->{$optName};
    }

    $WORKING_DIR = &SysUtils::fullPath($WORKING_DIR);
    if ($HELP) {
      &printUsage();
    }
  }

  return ($opts, $args);
}

sub createPartDir {
  my $partNo = shift;
  my $partDir = &partDir($partNo);
  my $err = &SysUtils::makePath($partDir);
  die($err) if $err;
  $err = &SysUtils::delDir($partDir, 1) if !$DEBUG;
  die($err) if $err;
  return $partDir;
}

sub partDir {
  my $partNo = shift;
  return "$WORKING_DIR${SysUtils::DIRSEP}$partNo";
}

sub partRecPath {
  my $partNo = shift;
  return &partDir($partNo) . $SysUtils::DIRSEP . 'audiobook.flac';
}

sub printUsage {
  my $err = shift;
  if ($err) {
    print STDERR ("$err");
  }
  &{$PRINT_USAGE}();
  if ($err) {
    exit(1);
  }
  else {
    exit(0);
  }
}

sub report {
  if ($VERBOSE) {
    print(@_);
  }
}

1;
