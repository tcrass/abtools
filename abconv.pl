#!/usr/bin/perl

#----------------------------------------------------------------------
#
# abconv.pl -- Torsten Crass, 2013 - 2015
#
# Convert audiobook recorded with abrec.pl to mp3 or other formats
#
# Call 'abconv.pl --help' for more information.
#
# This program's homepage:
# http://www.tcrass.de/computer/tools/media/audiobook
#
# LICENSE
# -------
#
# This program is distributed under the terms of the GNU GPL,
# version 2 (see http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
#
#----------------------------------------------------------------------


use strict;
use warnings;

use POSIX ':sys_wait_h';
use JSON::PP;

use SysUtils;
use ABTools;

my $SILENCE_DETECTION_MIN_DURATION = 0.1;

my $AUTHOR = undef;
my $AUTO_CHAPTERS = 0;
my $BOOK = undef;
my $CHAPTER_OFFSET = -1;
my $CONVERT_CMD = 'ffmpeg -y -i "%i" -ar %r -ab 96k "%o" 2>&1 > /dev/null';
my $DURATIONS = undef;
my $FILENAME_PATTERN = '%0n - %b %n.mp3';
my $FIX_CHAPTERS = 0;
my $INCLUDE_COVER_ART = 0;
my $OUT_DIR = './out';
my $REUSE_SILENCE = 0;
my $REMOVE_INTRO = 0;
my $SILENCE_LEVEL = '0.5%';
my $SILENCE_MIN_DURATION = 0.2;
my $TAGGING_CMD = 'id3v2 -2 -a "%a" -A "%a - %b" -t "%0n - %b %n/%N" -T %0n "%o" 2>&1 > /dev/null';
my $ADD_COVER_ART_CMD = 'eyeD3 -2 --add-image="%c":FRONT_COVER "%o" 2>&1 > /dev/null';

my $OPT_DEFS = {
  'auto-chapters|a=' => \$AUTO_CHAPTERS,
  'author|A=' => \$AUTHOR,
  'book|B=' => \$BOOK,
  'include-cover-art|C' => \$INCLUDE_COVER_ART,
  'durations|d=' => \$DURATIONS,
  'remove-intro|i=' => \$REMOVE_INTRO,
  'chapter-offset|f=' => \$CHAPTER_OFFSET,
  'silence-level|l=' => \$SILENCE_LEVEL,
  'convert-cmd|m=' => \$CONVERT_CMD,
  'out-dir|o=' => \$OUT_DIR,
  'filename-pattern|p=' => \$FILENAME_PATTERN,
  'reuse-silence|r' => \$REUSE_SILENCE,
  'silence-min-duration|s=' => \$SILENCE_MIN_DURATION,
  'tagging-cmd|t=' => \$TAGGING_CMD,
  'add-cover-art-cmd|T=' => \$ADD_COVER_ART_CMD,
  'fix-chapters|x=' => \$FIX_CHAPTERS
};

(my $opts, my $args) = ABTools::init($OPT_DEFS, \&printUsage);

if ($SILENCE_LEVEL =~ m/^(.+)%\s*$/) {
  $SILENCE_LEVEL = $1/100;
}
$OUT_DIR = &SysUtils::fullPath($OUT_DIR);

my @PARTS = @{$args};
unless (@PARTS) {
  &ABTools::printUsage("You must specify the path to at least one aax file!\n");
}

if ($AUTO_CHAPTERS && $FIX_CHAPTERS) {
  &ABTools::printUsage("The -a and -x options are mutually exclusive!\n");
}

my @DURATIONS = &parseDurations($DURATIONS);
if (@DURATIONS && scalar(@DURATIONS) != scalar(@PARTS)) {
  &ABTools::printUsage("When using -D, you must specify exactly as many durations as there are parts in the audiobook!\n");
}

my $JSON = JSON::PP->new()->pretty();

my $BOOK_DESCR = &getBookDescription();

my $PARTS_DESCR = &getPartsDescr($BOOK_DESCR);


&findPartsChapters($PARTS_DESCR);

&splitPartsIntoChapters($PARTS_DESCR);

&postProcessChapters($PARTS_DESCR);

&convertToTargetFormat($PARTS_DESCR);


#=== End of main program ==============================================


sub parseDurations {
  my @durations = ();
  if ($DURATIONS) {
    my @parts = split(/\s*,\s*/, $DURATIONS);
    foreach my $part (@parts) {
      my $duration = &parseTimeStr($part);
      push(@durations, $duration);
    }
  }
  return @durations;
}


#--- Get book description ---------------------------------------------


sub getBookDescription {
  &report("Retrieving book description...\n");
  my $path = $PARTS[0];
  my @cmd = ('mp4info', $path);
  (my $out, my $err) = &SysUtils::execCommand(@cmd);
  die($err) if $err;
  $out = ${$out};

  my $bookDescr = {
    author => '<unknown>',
    book => '<unknown>',
    hasCoverArt => 0,
    isMultipart => scalar(@PARTS) > 1
  };

  if ($out =~ m/^\s+Artist:\s*(.+)?\s*$/m) {
    $bookDescr->{author} = $1;
  }
  if ($AUTHOR) {
    $bookDescr->{author} = $AUTHOR;
  }

  if ($out =~ m/^\s+Album:\s*(.+)?\s*$/m) {
    $bookDescr->{book} = $1;
  }
  if ($BOOK) {
    $bookDescr->{book} = $BOOK;
  }
  &report('  Author: ', $bookDescr->{author}, "\n  Book: ", $bookDescr->{book}, "\n");

  if ($out =~ m/^\s+Cover Art pieces:\s*(.+)?\s*$/m) {
    $bookDescr->{hasCoverArt} = $1;
    if ($INCLUDE_COVER_ART) {
      $bookDescr->{coverArtPath} = &extractCoverArt();
    }
  }

  return $bookDescr;
}


sub extractCoverArt {
  &report(" Extracting cover art...\n");
  (my $dir, my $file, my $name, my $ext) = &SysUtils::getPathSegments($PARTS[0]);
  my $imgPattern = "$dir$SysUtils::DIRSEP$name.art*";
  my @img = glob($imgPattern);
  unlink(@img);

  my $path = $PARTS[0];
  my @cmd = (qw(mp4art --extract), $path);
  (my $out, my $err) = &SysUtils::execCommand(@cmd);
  die($err) if $err;

  @img = glob($imgPattern);
  my $img = $img[0];
  my $coverArtPath = &ABTools::partDir(1) . $SysUtils::DIRSEP . 'coverArt.jpg';
  @cmd = (qw(convert), $img, $coverArtPath);
  ($out, $err) = &SysUtils::execCommand(@cmd);
  die($err) if $err;

  unlink(@img) unless $DEBUG;
  return $coverArtPath;
}

#--- Get parts's descriptions -----------------------------------------


sub getPartsDescr {
  my $bookDescr = shift;
  &report("Retrieving parts information (from aax files)...\n");
  my @partsDescr = ();
  for (my $i = 0; $i < scalar(@PARTS); $i++) {
    my $partDescr = &getPartDescr($i + 1);
    $partDescr->{bookDescr} = $bookDescr;
    push(@partsDescr, $partDescr);
  }
  $bookDescr->{partsDescr} = \@partsDescr;
  return \@partsDescr;
}

sub getPartDescr {
  my $partNo = shift;
  my $partDescr = &getPartStats($partNo);
  if (@DURATIONS) {
    $partDescr->{nominalDuration} = $DURATIONS[$partNo-1];
  }
  else {
    $partDescr->{nominalDuration} = $partDescr->{duration};
  }
  return $partDescr;
}

sub getPartStats {
  my $partNo = shift;
  my $recPath = &ABTools::partRecPath($partNo);
  my $stats = &getAudioStats($recPath);
  $stats->{partNo} = $partNo;
  $stats->{recPath} = $recPath;
  return $stats;
}


#--- Find parts's chapters --------------------------------------------


sub findPartsChapters {
  &report("Finding parts' chapters...\n");
  my $partsDescr = shift;
  for (my $i = 0; $i < scalar(@PARTS); $i++) {
    my $partDescr = $partsDescr->[$i];
    &report('  Part ', $i + 1, ' has a duration of ', &toTimeStr($partDescr->{duration}), " seconds.\n");
    if ($AUTO_CHAPTERS) {
      &autoSplitChapters($partDescr);
      &report('  Part is auto-split into ', scalar(@{$partDescr->{chapters}}), " chapters.\n");
    }
    else {
      &findPartChapters($partDescr);
      &report('  Part is split into ', scalar(@{$partDescr->{chapters}}), " chapters.\n");
      if ($FIX_CHAPTERS) {
        &fixChapters($partDescr);
      }
    }
  }
}

sub autoSplitChapters {
  my $partDescr = shift;
  &report('  Auto-finding chapter positions in part ', $partDescr->{partNo}, "...\n");
  my $silences = &findSilences($partDescr);
  &report('    Found ', scalar(@{$silences}), " bits of silence.\n");
  my @partChapters = ();
  my $n = 0;
  my $lastChapterStart = 0;
  foreach my $silence (@{$silences}) {
    if (($silence->{duration} >= $SILENCE_MIN_DURATION) &&
        (($silence->{breakpoint} - $lastChapterStart) > $AUTO_CHAPTERS))
    {
      $n++;
      my $chapter = {
        partDescr => $partDescr,
        partNo => $partDescr->{partNo},
        chapterNo => $n,
        start => $lastChapterStart,
        name => undef,
        path => undef,
        end => undef,
        duration => undef
      };
      push(@partChapters, $chapter);
      $lastChapterStart = $silence->{breakpoint};
    }
  }
  $n++;
  my $chapter = {
    partDescr => $partDescr,
    partNo => $partDescr->{partNo},
    chapterNo => $n,
    start => $lastChapterStart,
    name => undef,
    path => undef,
    end => undef,
    duration => undef
  };
  push(@partChapters, $chapter);
  $partDescr->{chapters} = \@partChapters;
}

sub findPartChapters {
  my $partDescr = shift;
  my $path = $PARTS[$partDescr->{partNo} - 1];
  my @cmd = ('mp4chaps', '--list', $path);
  (my $out, my $err) = &SysUtils::execCommand(@cmd);
  die($err) if $err;
  $out = ${$out};
  my @partChapters = ();
  my $n = 0;
  while ($out =~ m/^\s+Chapter.+-\s+(\d+:\d+:\d+\.\d+)\s+-\s+"(.+)"\s*$/mg) {
    $n++;
    my $chapter = {
      partDescr => $partDescr,
      partNo => $partDescr->{partNo},
      chapterNo => $n,
      start => &parseTimeStr($1) + $CHAPTER_OFFSET,
      name => $2,
      path => undef,
      end => undef,
      duration => undef
    };
    if ($chapter->{start} < 0) {
      $chapter->{start} = 0;
    }
    push(@partChapters, $chapter);
  }
  $partDescr->{chapters} = \@partChapters;
}

sub fixChapters {
  my $partDescr = shift;
  &report('  Fixing chapter positions in part ', $partDescr->{partNo}, "...\n");
  my $silences = &findSilences($partDescr);
  &report('    Found ', scalar(@{$silences}), " bits of silence.\n");
  for (my $i = 1; $i < scalar(@{$partDescr->{chapters}}); $i++) {
    my $chapter = $partDescr->{chapters}->[$i];
    my $chapterStart = $chapter->{start} * $partDescr->{duration} / $partDescr->{nominalDuration};
    my $silence = &findLongestSilenceAround($silences, $chapterStart, $FIX_CHAPTERS);
    if ($silence) {
      &report('    Moving start of chapter ', $i + 1, ' from ', &toTimeStr($chapter->{start}));
      $chapter->{start} = $silence->{breakpoint};
      &report(' to ', &toTimeStr($chapter->{start}), ".\n");
    }
  }
}

sub findSilences {
  my $partDescr = shift;

  my $silences = [];
  if ($REUSE_SILENCE) {
    $silences = &readSilences($partDescr);
  }
  else {
    &report("    Scanning for bits of silence... (Note: This may take a LONG time!)\n");

    my @cmd = ('sox', $partDescr->{recPath},  '-b', '16', '-t', 'raw', '-', 'rate', $partDescr->{sampleRate});

    (my $pid, my $err, my $raw) = &SysUtils::execInBackground(@cmd);
    die($err) if $err;

    my $threshold = int(2**$partDescr->{sampleBits} * $SILENCE_LEVEL / 2);
    my $minSilenceSamples = int($SILENCE_DETECTION_MIN_DURATION * $partDescr->{sampleRate});
    my $n = 0;
    my $t0 = time();
    my $silence;
    while (!eof($raw)) {
      my $level = &readNextAvgLevel($raw, $partDescr);
      if ($level < $threshold) {
        unless ($silence) {
          $silence = {
            start => $n,
          };
        }
      }
      else {
        if ($silence) {
          $silence->{end} = $n - 1;
          $silence->{duration} = $silence->{end} - $silence->{start};
          if ($silence->{duration} > $minSilenceSamples) {
            $silence->{start} = ($silence->{start} / $partDescr->{sampleRate});
            $silence->{end} = ($silence->{end} / $partDescr->{sampleRate});
            $silence->{duration} = ($silence->{duration} / $partDescr->{sampleRate});
            $silence->{breakpoint} = $silence->{end} - $SILENCE_MIN_DURATION/2;
            push(@{$silences}, $silence);
          }
          $silence = undef;
        }
      }
      $n++;
      if ($n % 1e7 == 0) {
        my $tPart = $n / $partDescr->{sampleRate};
        my $fPart = $tPart/$partDescr->{duration};
        my $t = time();
        my $dt = $t - $t0;
        my $total = $dt/$fPart;
        my @eta = localtime($t0 + $total);
        my $eta = "$eta[2]:$eta[1]:$eta[0]";
        &report('    At ', &toTimeStr($tPart), ' of part ', $partDescr->{partNo}, ': ', $n/1e6, ' million samples processed (', sprintf('%04.1f', 100*$fPart), "%, ETA $eta)\n");
      }
    }

    waitpid($pid, WNOHANG);

  }

  if (!$REUSE_SILENCE) {
    &writeSilences($partDescr, $silences);
  }

  my $minDurationSilences = &filterSilencesForMinDuration($partDescr, $silences);

  return $minDurationSilences;
}

sub readNextAvgLevel {
  my $raw = shift;
  my $partDescr = shift;
  my $buffer;
  my $avgLevel = 0;
  for (my $i = 0; $i < $partDescr->{channels}; $i++) {
    read($raw, $buffer, 2);
    my $val = unpack('s', $buffer);
    $avgLevel += $val;
  }
  $avgLevel /= $partDescr->{channels};
  return abs($avgLevel);
}

sub readSilences {
  my $partDescr = shift;

  my $path = &getSilencePath($partDescr);
  &report("    Reading silence data from $path...\n");

  (my $str, my $err) = &SysUtils::readFromFile($path);
  die($err) if $err;

  return $JSON->decode(${$str});
}

sub writeSilences {
  my $partDescr = shift;
  my $silences = shift;

  my $path = &getSilencePath($partDescr);
  &report("    Writing silence data to $path...\n");

  my $err = &SysUtils::writeToFile($path, $JSON->encode($silences));
  die($err) if $err;
}

sub getSilencePath {
  my $partDescr = shift;
  return   "$WORKING_DIR${SysUtils::DIRSEP}silences" . $partDescr->{partNo} . ".json";
}

sub filterSilencesForMinDuration {
  my $partDescr = shift;
  my $silences = shift;

  &report("    Filtering for bits of silence at least $SILENCE_MIN_DURATION s long...\n");

  my @minDurationSilences = ();
  foreach my $silence (@{$silences}) {
    if ($silence->{duration} >= $SILENCE_MIN_DURATION) {
      push(@minDurationSilences, $silence);
    }
  }

  return \@minDurationSilences;
}

sub findLongestSilenceAround {
  my $silences = shift;
  my $pos = shift;
  my $eps = shift;

  my $from = $pos - $eps;
  my $to = $pos + $eps;

  my $silence = undef;
  my $minDelta = undef;
  foreach my $cand (@{$silences}) {
    if (($cand->{breakpoint} >= $from) && ($cand->{breakpoint} <= $to)) {
      if ($silence) {
        my $delta = abs($pos - $cand->{breakpoint});
        if ($cand->{duration} > $silence->{duration} || ($cand->{duration} == $silence->{duration} && $delta < $minDelta)) {
          $silence = $cand;
          $minDelta = $delta;
        }
      }
      else {
        $silence = $cand;
        $minDelta = abs($pos - $cand->{breakpoint});
      }
    }
  }
  return $silence;
}


#--- Split parts into chapters ----------------------------------------


sub splitPartsIntoChapters {
  my $partsDescr = shift;
  for (my $i = 0; $i < scalar(@{$partsDescr}); $i++) {
#  for (my $i = 0; $i < 1; $i++) {
    &report('Splitting part ', $i + 1, " into chapters...\n");
    my $partDescr = $partsDescr->[$i];
    &splitPartIntoChapters($partDescr);
  }
}

sub splitPartIntoChapters {
  my $partDescr = shift;
  my $partDir = &ABTools::partDir($partDescr->{partNo});
  unlink(glob("$partDir${SysUtils::DIRSEP}*.wav"));
  my @chapters = @{$partDescr->{chapters}};
  for (my $i = 0; $i < scalar(@chapters); $i++) {
#  for (my $i = 0; $i < 1; $i++) {
    my $chapter = $chapters[$i];
    if ($i < scalar(@chapters) - 1) {
      $chapter->{end} = $chapters[$i + 1]->{start};
    }
    else {
      $chapter->{end} = $partDescr->{duration};
    }
    $chapter->{duration} = $chapter->{end} - $chapter->{start};

    &extractChapter($chapter);
  }
}

sub extractChapter {
  my $chapter = shift;
  my $partDir = &ABTools::partDir($chapter->{partNo});
  my $partRecPath = &ABTools::partRecPath($chapter->{partNo});

  my $partStr = sprintf('%02i', $chapter->{partNo});
  my $chapterStr = sprintf('%04i', $chapter->{chapterNo});

  my $start = $chapter->{start};
  my $end = $chapter->{end};

  my $chapterPath = "$partDir${SysUtils::DIRSEP}$partStr.$chapterStr.wav";
  $chapter->{path} = $chapterPath;

  &report('  Extracting chapter ', $chapter->{chapterNo}, " to $chapterPath\n");

  my @cmd = ('sox', $partRecPath, $chapterPath, 'trim', &toTimeStr($start), '=' . &toTimeStr($end));
  (my $out, my $err) = &SysUtils::execCommand(@cmd);
  die($err) if $err;
}


#--- Post-process chapters --------------------------------------------


sub postProcessChapters {
  &report("Post-processing chapters...\n");
  my $partsDescr = shift;
  my $partsCount = scalar(@{$partsDescr});
  for (my $i = 0; $i < scalar(@{$partsDescr}); $i++) {
    &report('  Post-processing chapters of part ', $i + 1, "...\n");
    my $partDescr = $partsDescr->[$i];
    if ($REMOVE_INTRO) {
      &report('    Removing intro from part ', $i + 1, "...\n");
      my $chap1 = $partDescr->{chapters}->[0];

      my $outPath = &removeIntro($chap1->{path});
      &SysUtils::moveFile($outPath, $chap1->{path});
      $outPath = &padWithSilence($chap1->{path}, {before => 1});
      &SysUtils::moveFile($outPath, $chap1->{path});

      my $lastChap = $partDescr->{chapters}->[scalar(@{$partDescr->{chapters}}) - 1];
      $outPath = &removeIntro($lastChap->{path}, {reverse => 1});
      &SysUtils::moveFile($outPath, $lastChap->{path});
      if (($partsCount > 1) && ($i < $partsCount - 1)) {
        $outPath = &removeIntro($lastChap->{path}, {reverse => 1});
        &SysUtils::moveFile($outPath, $lastChap->{path});
      }

      my $lastStats = &getAudioStats($lastChap->{path});
      if ($lastStats->{duration} == 0) {
        pop(@{$partDescr->{chapters}});
      }
      else {
        $outPath = &padWithSilence($lastChap->{path}, {after => 3});
        &SysUtils::moveFile($outPath, $lastChap->{path});
      }

    }
  }
}

sub removeIntro {
  my $path = shift;
  my $opts = shift || {};

  my $outPath = $opts->{out} || "$WORKING_DIR${SysUtils::DIRSEP}tmp.wav";

  my $silenceLevelPercent = (100 * $SILENCE_LEVEL) . '%';
  #my $silenceLevelPercent = '0.1%';

  my $stats = &getAudioStats($path);
  my $removeIntro = &toTimeStr($REMOVE_INTRO);
  my @cmd = (qw(sox), $path, $outPath);
  if ($opts->{reverse}) {
    push(@cmd, 'reverse');
  }
  push(@cmd, qw(silence 1 0.1), $silenceLevelPercent, 1, $removeIntro, $silenceLevelPercent, qw(reverse silence 1 0.1), $silenceLevelPercent);
  if (!$opts->{reverse}) {
    push(@cmd, 'reverse');
  }

  (my $out, my $err) = SysUtils::execCommand(@cmd);
  die($err) if ($err);

  my $introStats = &getAudioStats($outPath);

#    print("@cmd\n");
#    &SysUtils::printRefDump($introStats);


  my $trim = &toTimeStr($introStats->{duration});
  @cmd = (qw(sox), $path, $outPath);
  if ($opts->{reverse}) {
    push(@cmd, 'reverse');
  }
  push(@cmd, (qw(trim), $trim, qw(silence 1 0.2), '0.1%'));
  if ($opts->{reverse}) {
    push(@cmd, 'reverse');
  }

#    print("@cmd\n");

  ($out, $err) = SysUtils::execCommand(@cmd);
  die($err) if ($err);

  return $outPath;
}

sub padWithSilence {
  my $path = shift;
  my $opts = shift || {};

  my $outPath = $opts->{out} || "$WORKING_DIR${SysUtils::DIRSEP}tmp.wav";
  my $before = $opts->{before} || 0;
  my $after = $opts->{after} || 0;

  my @cmd = (qw(sox), $path, $outPath, qw(pad), $before, $after);
  (my $out, my $err) = &SysUtils::execCommand(@cmd);
  die($err) if $err;

  return $outPath;
}


#--- Convert to target format


sub convertToTargetFormat {
  my $partsDescr = shift;

  &report("Converting chapters to target format...\n");

  my $err = &SysUtils::makePath($OUT_DIR);
  die($err) if $err;
  $err = &SysUtils::delDir($OUT_DIR, 1);
  die($err) if $err;

  my $tracksCount = 0;
  for (my $i = 0; $i < scalar(@{$partsDescr}); $i++) {
    my $partDescr = $partsDescr->[$i];
    for (my $j = 0; $j < scalar(@{$partDescr->{chapters}}); $j++) {
      $tracksCount++;
    }
  }
  my $trackFormatString = &getPaddedFormatString($tracksCount);

  my $trackNo = 1;
  my $partsCount = scalar(@{$partsDescr});
  my $partFormatString = &getPaddedFormatString($partsCount);
  for (my $i = 0; $i < $partsCount; $i++) {
    my $partDescr = $partsDescr->[$i];
    my $partNo = $i + 1;
    my $chaptersCount = scalar(@{$partDescr->{chapters}});
    my $chapterFormatString = &getPaddedFormatString($chaptersCount);
    for (my $j = 0; $j < $chaptersCount; $j++) {
      my $chapter = $partDescr->{chapters}->[$j];
      my $chapterNo = $j + 1;
      my $data = {
        author => $BOOK_DESCR->{author},
        book => $BOOK_DESCR->{book},
        partNo => $partNo,
        paddedPartNo => sprintf($partFormatString, $partNo),
        partsCount => $partsCount,
        chapterNo => $chapterNo,
        paddedChapterNo => sprintf($chapterFormatString, $chapterNo),
        chaptersCount => $chaptersCount,
        chapterName => $chapter->{name},
        trackNo => $trackNo,
        paddedTrackNo => sprintf($trackFormatString, $trackNo),
        tracksCount => $tracksCount
      };
      if ($INCLUDE_COVER_ART) {
        $data->{coverArtPath} = $partDescr->{bookDescr}->{coverArtPath};
      }

      my $filename = &substituteTrackData($FILENAME_PATTERN, $data);
      my $outPath = &SysUtils::fullPath("$OUT_DIR$SysUtils::DIRSEP$filename");
      &report('  Converting ', $chapter->{path}. " to $outPath...\n");

      my $cmd = &substituteSampleData($CONVERT_CMD, $partDescr);
      $cmd =~ s/%i/$chapter->{path}/g;
      $cmd =~ s/%o/$outPath/g;
      (my $out, my $err) = &SysUtils::execCommand($cmd);
      die($err) if $err;

      unlink($chapter->{path})  unless $DEBUG;

      if ($TAGGING_CMD) {
        &report("    Tagging $outPath...\n");
        $cmd = &substituteTrackData($TAGGING_CMD, $data);
        $cmd =~ s/%o/$outPath/g;
        ($out, $err) = &SysUtils::execCommand($cmd);
        die($err) if $err;

        if ($INCLUDE_COVER_ART) {
          &report("      Adding cover art...\n");
          $cmd = &substituteTrackData($ADD_COVER_ART_CMD, $data);
          $cmd =~ s/%o/$outPath/g;
          ($out, $err) = &SysUtils::execCommand($cmd);
          die($err) if $err;
        }

      }

      $trackNo++;
    }
  }

}

sub substituteSampleData {
  my $pattern = shift;
  my $sampleData = shift;
  $pattern =~ s/%n/$sampleData->{channels}/g;
  $pattern =~ s/%b/$sampleData->{sampleBits}/g;
  $pattern =~ s/%r/$sampleData->{sampleRate}/g;
  return $pattern;
}

#--- Common subroutines -----------------------------------------------


sub getAudioStats {
  my $path = shift;
  my @cmd = ('soxi', $path);
  (my $out, my $err) = &SysUtils::execCommand(@cmd);
  die($err) if $err;
  $out = ${$out};

  my $stats = {
    duration => 0,
    sampleBits => 0,
    sampleRate => 0,
    channels => 0,
  };
  if ($out =~ m/^\s*Duration\s*:\s*(\S+)/m) {
    $stats->{duration} = &parseTimeStr($1);
  }
  if ($out =~ m/^\s*Channels\s*:\s*(\d+)/m) {
    $stats->{channels} = $1;
  }
  if ($out =~ m/^\s*Sample Rate\s*:\s*(\d+)/m) {
    $stats->{sampleRate} = $1;
  }
  if ($out =~ m/^\s*Precision\s*:\s*(\d+)/m) {
    $stats->{sampleBits} = $1;
  }
  return $stats;
}

sub getPaddedFormatString {
  my $n = shift;
  return '%0' . length($n + 0) . 'u';
}

sub substituteTrackData {
  my $pattern = shift;
  my $data = shift;
  $pattern =~ s/%a/$data->{author}/g;
  $pattern =~ s/%b/$data->{book}/g;
  if ($INCLUDE_COVER_ART) {
    $pattern =~ s/%c/$data->{coverArtPath}/g;
  }
  $pattern =~ s/%p/$data->{partNo}/g;
  $pattern =~ s/%0p/$data->{paddedPartNo}/g;
  $pattern =~ s/%P/$data->{partsCount}/g;
  $pattern =~ s/%m/$data->{chapterNo}/g;
  $pattern =~ s/%0m/$data->{paddedChapterNo}/g;
  $pattern =~ s/%M/$data->{chaptersCount}/g;
  $pattern =~ s/%e/$data->{chapterName}/g;
  $pattern =~ s/%n/$data->{trackNo}/g;
  $pattern =~ s/%0n/$data->{paddedTrackNo}/g;
  $pattern =~ s/%N/$data->{tracksCount}/g;
  return $pattern;
}

sub parseTimeStr {
  my $str = shift;
  my $t = 0;
  if ($str =~ m/^\s*(\d+):(\d+):(\d+)(.(\d+))?\s*$/) {
    $t = 3600 * $1 + 60 * $2 + 1 * $3;
    if ($5) {
      $t += $5 / ('1' . ('0' x length($5)));
    }
  }
  return $t;
}

sub toTimeStr {
  my $t = shift;
  my $hours = sprintf('%02u', int($t / 3600));
  $t -= 3600 * $hours;
  my $mins = sprintf('%02u', int($t / 60));
  $t -= 60 * $mins;
  my $secs = sprintf('%06.3f', $t);
  return "$hours:$mins:$secs";
}

sub printUsage {
  print <<EOT;

abconv.pl -- Convert audiobook recorded with abrec.pl to mp3 or
             other formats
Version $VERSION
-----------------------------------------------------------------------
Usage: abconv.pl [options] <part1.aax> [<part2.aax> ...]
with possible options:
-?|--help           : Show this message.
-a|--auto-chapters <f>: If != 0, split into chapters at every piece
                      of silence of at least -s seconds, ensuring
                      a minimum chapter duration of <f> seconds.
                      Mutually exclusive with -x. [$AUTO_CHAPTERS]
-A|--author <s>     : Use <s> as the book's author's name, rather than
                      what is stored in the original aax file.
-B|--book <s>       : Use <s> as the book's title, rather than what is
                      stored in the original aax file.
-c|--config-path <p>: Path to an alternative config file.
-C|--include-cover-art: If available, extract cover art from aax file
                      and include in audio files (using -T).
-d|--durations <d>  : Comma-separated list of durations of all the
                      audiobook's parts (according to publisher). Must
                      be given in h:m:s format and will be taken into
                      account when fixing chapters (see -x option), if
                      specified.
-D|--debug          : Don't delete some temporary audio files.
-i|--remove-intro <f>: If != 0, remove intro and goodbye comments (as
                      well as the 'This is the end of part x' message
                      found in multipart books) using <f> seconds of
                      silence as end-of-message marker. [$REMOVE_INTRO]
-f|--chapter-offset <f>: Split into chapters <f> seconds off the
                      chapter marks stored in the aax file. [$CHAPTER_OFFSET]
-m|--convert-command <c>: Execute <c> in order to convert a chapter wav
                      to the desired target format. Apart from audio
                      format placeholders (see below), the following
                      placeholders can (and should) be used: %i = path
                      to input file; %o = path to output file.
                      [$CONVERT_CMD]
-o|--out-dir <p>    : Path to output directory, which will eventually
                      contain the converted audio files.
                      [$OUT_DIR]
-p|--filename-pattern <s>: Template for the converted audio files'
                      names. All track data placeholders (see below)
                      are understood. [$FILENAME_PATTERN]
-r|--reuse-silence  : When When auto-splitting into chapters (see -a)
                      or fixing chapter positions (see -x), re-use
                      silence information gathered in a previous run.
-l|--silence-level <l>: Maximum level to be considered as silence.
                      Can be specified either as relative value in
                      the (0, 1) interval or as percent value. [$SILENCE_LEVEL]
-s|--silence-min-duration <f>: When auto-splitting into chapters (see
                      -a) or fixing chapter positions (see -x), only
                      treat intevals of silence of at least <f>
                      seconds in length as chapter breakpoint
                      candidates. [$SILENCE_MIN_DURATION]
-t|--tagging-command <c>: Execute <c> for tagging the converted audio
                      files. Apart from all track data placeholders,
                      the %i placeholder (= path to audio file) can
                      (and should) be used.
                      [$TAGGING_CMD]
-T|--add-cover-art-command <c>: Execute <c> for adding cover art to
                      converted audio files; will be used, if the -C
                      option has been passed.
                      [$ADD_COVER_ART_CMD]
-x|--fix-chapters <f>: If != 0, instead of splitting the recording at
                      the chapter breaktpoints stored in the original
                      aax file, split at the longest interval of
                      silence found within +/- <f> seconds around the
                      aax chapter positions. Mutually exclusive with
                      -a. [$FIX_CHAPTERS]
-v|--verbose        : Talk a lot.
-w|--working-dir    : Where the recorded flac files are stored.
                      [$WORKING_DIR]
Audio format placeholders:
  %b = Bitdepth of recording (per channel)
  %r = Bitrate of recording (per channel)
  %n = Number of channels in recording
Track data placeholders:
  %a  = Author
  %b  = Book title
  %c  = Path to cover art image file
  %p  = Part number
  %0p = Part number padded with zeros
  %P  = Parts count
  %m  = Chapter number (within part)
  %0m = Chapter number padded with zeros
  %M  = Chapters count
  %e  = Chapter name (from aax file)
  %n  = Track number (over all parts)
  %0n = Track number padded with zeors
  %N  = Tracks count
Requires ABTools.pm and SysUtils.pm to be in PERL5LIB and relies
on sox as well as the mp4v2-utils package, which should be installed
and in PATH.
In standard configuration, audio file conversion is performed using
ffmpeg, while tagging relies on eyeD3.

EOT
}
